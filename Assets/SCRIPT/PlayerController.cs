﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("CONFIG NUMBER")]
    [SerializeField]
    float LengHitCollider = 0;
    [SerializeField]
    float _Speed = 0,_Jump_Force = 0,Config_slope_force = 0,Config_Fall = 0,
    HangCheckLeng = 0;
    public float Speed => _Speed;

    Rigidbody2D Rigi = null;
    Collider2D Collider = null;
    [SerializeField]
    Transform Avatar = null;
    [SerializeField]
    Transform[] HangCheckPos = new Transform[2]; 
    [Header("RUNTIME INFO")]
    [SerializeField]
    PlayState PLAYER_STATE = PlayState.IDLE;
    [SerializeField]
    bool OnGround = false,OnHang = false;
    void Awake(){
        Rigi = this.GetComponent<Rigidbody2D>();
        Collider = this.GetComponent<Collider2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckOnGround();
        CheckHang();
        DefineState();
        
    }

    void FixedUpdate(){
        this.Move();
       
    }

    void Move(){
 
        if(OnHang){
            if(Input.GetAxisRaw("Horizontal") * Avatar.localScale.x > 0 || Input.GetAxisRaw("Vertical") > 0){
                //user climb hang
                //RunAnimClimb
                //Set new pos
                int way = Avatar.localScale.x > 0 ? 1:-1;
                Vector2 newPos = this.transform.position;
                newPos.x += Mathf.Abs(Avatar.localScale.x /2) * 1 * way;
                newPos.y += Avatar.localScale.y / 2 * 1 ;

                this.transform.position = newPos;
                Rigi.gravityScale = 1;
            }
            if(Input.GetAxisRaw("Horizontal") * Avatar.localScale.x < 0 || Input.GetAxisRaw("Vertical") < 0){
                //user release hang
                Rigi.gravityScale = 1;
            }
            return;
        }

        Rigi.gravityScale = 1;
        if(OnGround)
            Rigi.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * _Speed * Time.deltaTime, Rigi.velocity.y);

        if (Input.GetAxisRaw("Vertical") > 0 && OnGround)
        {
            Rigi.velocity = new Vector2(Rigi.velocity.x,0);
            Rigi.AddForce(new Vector2(0, _Jump_Force));
            OnGround = false;
        }

        //Add force on fall
        if(Rigi.velocity.y < 0 ){
            //Rigi.AddForce(new Vector2(0,Physics2D.gravity.y * Config_Fall));
            Rigi.velocity += Vector2.up * Physics2D.gravity.y *Config_Fall * Time.deltaTime;
            
        }
        
        //Rotate face
        if(Rigi.velocity.x < 0){
            Avatar.localScale = new Vector2(-Mathf.Abs(Avatar.localScale.x),Avatar.localScale.y);
        }

        if(Rigi.velocity.x > 0){
            Avatar.localScale = new Vector2(Mathf.Abs(Avatar.localScale.x),Avatar.localScale.y);
        }
    }

    void CheckOnGround(){
        RaycastHit2D[] hits = new RaycastHit2D[10];
        bool isInSlope = false;
        bool isOnGround = false;
        int numHits = Collider.Cast(Vector2.down,hits,LengHitCollider,true);
        foreach(RaycastHit2D r in hits){
            if(r.collider != null){
                isOnGround = true;
                OnHang = false;
            }

            if(r.normal != Vector2.up && r.normal != Vector2.zero){
                isInSlope = true;
            }

        }
        OnGround = isOnGround;
        if(isInSlope){
            Rigi.AddForce(new Vector2(0,-Config_slope_force));
        }
            
    }
    void CheckHang(){
        if(OnHang)
            return;
        RaycastHit2D[] hits = new RaycastHit2D[HangCheckPos.Length];
        Vector2 way = Avatar.localScale.x < 0 ? Vector2.left : Vector2.right;
        for(int i = 0; i< hits.Length; i++){
            hits[i] = Physics2D.Raycast(HangCheckPos[i].position,way,HangCheckLeng);
            Debug.DrawRay(HangCheckPos[i].position,way,Color.blue);
        }
        if(hits[0].collider == null && hits[1].collider != null){
            OnHang = true;
            Rigi.velocity = Vector2.zero;
            Rigi.gravityScale = 0;
        }else{
            OnHang = false;
        }
    }

    void DefineState(){
        if(OnGround){
            if(Rigi.velocity.x != 0){
                PLAYER_STATE = PlayState.RUN;
            }else{
                PLAYER_STATE = PlayState.IDLE;
            }
        }else{
            if(Rigi.velocity.y > 0){
                PLAYER_STATE = PlayState.JUMP;
            }
            if(Rigi.velocity.y < 0){
                PLAYER_STATE = PlayState.FALL;
            }
        }

        if(OnHang){
            PLAYER_STATE = PlayState.HANG;
        }
    }


    public enum PlayState{
        IDLE,
        RUN,
        HANG,
        JUMP,
        FALL
    }
}
